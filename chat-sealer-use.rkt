#lang racket

(require goblins
         goblins/actor-lib/methods
         goblins/actor-lib/cell
         goblins/actor-lib/actor-sealers)


;;; This isn't complete, and it doesn't show off how to make sealers
;;; that only apply to a particular channel, but that should be fine
;;; for this demonstration

(define (^chatroom _bcom)
  (define msg-log
    (spawn ^cell '()))
  (define (add-message from-user sealed-msg)
    ($ msg-log
       (cons `#hasheq([user . ,from-user]
                      [sealed-message . ,sealed-msg])
             ($ msg-log))))
  (methods
   ;; Just show all the messages we've gotten so far.
   [(message-log)
    ($ msg-log)]
   ;; A chatroom that recieves messages and rejects ones that appear
   ;; to not be by the right user.
   [(recv-msg from-user sealed-msg)
    ;; obviously inefficient in that we'd want to cache the user's
    ;; sealed? predicate
    (on (<- (<- from-user 'get-chat-sealed?) sealed-msg)
        (lambda (was-sealed?)
          (if was-sealed?
              (begin (add-message from-user sealed-msg)
                     'MESSAGE-OK)
              'DENIED))
        #:promise? #t)]
   ;; demonstrating a naive channel that accepts naive messages
   ;; luckily, chatroom participants can still discover that they're
   ;; BS!
   [(vulnerable-recv-msg from-user sealed-msg)
    (add-message from-user sealed-msg)
    'MESSAGE-OK]))


;;; Here we spawn a user and also an actor that can control that user
;;; Return both as a cons cell of (user . user-controller)
(define (spawn-user-controller-pair self-proposed-name
                                    #:debug? [debug? #f])
  (define-values (chat-msg-sealer chat-msg-unsealer chat-msg-sealed?)
    (spawn-sealer-triplet))
  ;; Here's our user object.  More or less it's a profile that provides
  ;; a self-proposed name, an unsealer, and a predicate to check whether
  ;; we sealed things
  (define (^user _bcom)
    (methods
     [(self-proposed-name)
      self-proposed-name]
     [(get-chat-sealed?)
      chat-msg-sealed?]
     [(get-chat-unsealer)
      chat-msg-unsealer]))
  (define user
    (spawn ^user))

  ;; Debugging code to show off whether or not our messages went through.
  (define (summarize-result msg-send-vow)
    (when debug?
      (on msg-send-vow
          (match-lambda
            ['MESSAGE-OK
             (displayln (format "DEBUG ~a: my message went through!"
                                self-proposed-name))]
            ['DENIED
             (displayln (format "DEBUG ~a: Aww, they rejected my message"
                               self-proposed-name))]
            [something-else
             (displayln (format "DEBUG ~a: Got some other weird message, wtf??? ~a"
                                self-proposed-name something-else))])
          #:catch
          (lambda (_err)
            (displayln (format "DEBUG ~a: Uh, it threw an error"))))))

  (define (^user-controller _bcom)
    (methods
     ;; Send a message to the chatroom, sealing with our sealer
     [(send-message to-chatroom contents)
      (summarize-result
       (<- to-chatroom 'recv-msg
           user ($ chat-msg-sealer contents)))]
     ;; Here we send a message pretending to be someone else.
     ;; The chatroom, if written correctly, will reject this one though.
     [(forge-message to-chatroom contents pretend-to-be)
      (summarize-result
       (<- to-chatroom 'recv-msg
           pretend-to-be ($ chat-msg-sealer contents)))]
     ;; Here we send a message using a vulnerable, incorrectly written
     ;; chatroom that doesn't check the sealer.  Nonetheless, those reading
     ;; the chatroom should be protected because this message should end up
     ;; rejected
     [(forge-message-using-vulnerability to-chatroom contents pretend-to-be)
      (summarize-result
       (<- to-chatroom 'vulnerable-recv-msg
           pretend-to-be ($ chat-msg-sealer contents)))]))
  (define user-controller
    (spawn ^user-controller))
  (cons user user-controller))

(define (read-the-room chatroom)
  (define (read-msg msg)
    (define user (hash-ref msg 'user))
    (define sealed-message (hash-ref msg 'sealed-message))
    (on (<- user 'self-proposed-name)
        (lambda (self-proposed-name)
          (on (<- (<- user 'get-chat-unsealer) sealed-message)
              (lambda (message-contents)
                (displayln
                 (format "<~a>: ~a" self-proposed-name message-contents)))
              #:catch
              (lambda (_err)
                (displayln "Hi!")
                (displayln
                 (format "WARNING: Couldn't unseal message from ~a... fraud may be afoot!"
                         self-proposed-name)))))))
  (on (<- chatroom 'message-log)
      (lambda (msgs)
        (for ([msg (reverse msgs)])
          (read-msg msg)))))

(module+ main
  (define a-vat
    (make-vat))
  (define b-vat
    (make-vat))
  (define c-vat
    (make-vat))
  (define m-vat
    (make-vat))

  (match-define (cons alice alice-controller)
    (a-vat 'run
           (lambda ()
             (spawn-user-controller-pair "Alice" #:debug? #t))))
  (match-define (cons bob bob-controller)
    (b-vat 'run
           (lambda ()
             (spawn-user-controller-pair "Bob" #:debug? #t))))
  (match-define (cons mallet mallet-controller)
    (m-vat 'run
           (lambda ()
             (spawn-user-controller-pair "Mallet" #:debug? #t))))
  (define chatroom
    (c-vat 'spawn ^chatroom))
  
  (a-vat 'call alice-controller 'send-message
         chatroom "What a wonderland this is")
  (b-vat 'call bob-controller 'send-message
         chatroom "You could also call me Robert, I suppose")
  (m-vat 'call mallet-controller 'forge-message
         chatroom "I'm definitely Alice lololololol" alice)
  (m-vat 'call mallet-controller 'forge-message-using-vulnerability
         chatroom "Okay this time I'm REALLY alice!!!!" alice)
  
  (sleep 0.5)
  (a-vat 'run
         (lambda ()
           (displayln "Now reading the room:")
           (read-the-room chatroom)))
  (sleep 0.5))
